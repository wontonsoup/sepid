#include "sepid-message.h"

struct _SepidMessage
{
    GObject parent_instance;

    gchar *message;

    gchar *prefix;
    gchar *command;
    GArray *parameters;
    gchar *trailing_parameter;

    SepidClient *sender;
    SepidClient *recipient;
};

G_DEFINE_TYPE (SepidMessage, sepid_message, G_TYPE_OBJECT)

enum
{
    PROP_MESSAGE = 1,
    PROP_SENDER,
    PROP_RECIPIENT,
    N_PROPERTIES
};

static GParamSpec *pspecs[N_PROPERTIES] = { 0 };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    SepidMessage *self;
    g_autofree GStrv tokens = NULL;
    gboolean has_prefix;
    gchar *trailing_parameter;

    self = SEPID_MESSAGE (object);

    switch (property_id)
    {
        case PROP_MESSAGE:
        {
            self->message = g_value_dup_string (value);

            tokens = g_strsplit (self->message, " ", 17);
            has_prefix = g_str_has_prefix (*tokens, ":");

            if (has_prefix)
            {
                self->prefix = g_strdup (tokens[0]);
                self->command = g_strdup (tokens[1]);
            }
            else
            {
                self->command = g_strdup (tokens[0]);
            }

            for (int i = 1 + has_prefix; tokens[i] != NULL; i++)
            {
                if (!g_str_has_prefix (tokens[i], ":"))
                {
                    gchar *parameter;

                    parameter = g_strdup (tokens[i]);

                    g_array_append_val (self->parameters, parameter);
                }
                else
                {
                    break;
                }
            }

            trailing_parameter =
                g_strstr_len (self->message + has_prefix, -1, ":");

            if (trailing_parameter != NULL)
            {
                gchar *parameter = NULL;

                parameter = g_strdup (trailing_parameter + 1);

                g_array_append_val (self->parameters, parameter);

                self->trailing_parameter = parameter;
            }
        }
        break;

        case PROP_SENDER:
        {
            self->sender = g_value_get_object (value);
        }
        break;

        case PROP_RECIPIENT:
        {
            self->recipient = g_value_get_object (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    SepidMessage *self;

    self = SEPID_MESSAGE (object);

    switch (property_id)
    {
        case PROP_MESSAGE:
        {
            g_value_set_string (value, self->message);
        }
        break;

        case PROP_SENDER:
        {
            g_value_set_object (value, self->sender);
        }
        break;

        case PROP_RECIPIENT:
        {
            g_value_set_object (value, self->recipient);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    SepidMessage *self;

    self = SEPID_MESSAGE (object);

    g_clear_pointer (&self->message, g_free);
    g_clear_pointer (&self->prefix, g_free);
    g_clear_pointer (&self->command, g_free);
    g_array_free (self->parameters, TRUE);
    self->trailing_parameter = NULL;
}

static void
sepid_message_class_init (SepidMessageClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    pspecs[PROP_MESSAGE] =
        g_param_spec_string ("message", "Message",
                             "Message",
                             NULL,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    pspecs[PROP_SENDER] =
        g_param_spec_object ("sender", "Sender",
                             "Sender",
                             SEPID_TYPE_CLIENT,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    pspecs[PROP_RECIPIENT] =
        g_param_spec_object ("recipient", "Recipient",
                             "Recipient",
                             SEPID_TYPE_CLIENT,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties (object_class, N_PROPERTIES, pspecs);
}

static void
sepid_message_init (SepidMessage *self)
{
    self->prefix = NULL;
    self->command = NULL;
    self->parameters = g_array_new (TRUE, FALSE, sizeof (gchar *));
    self->trailing_parameter = NULL;
}

gchar *
sepid_message_get_message (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->message;
}

gchar *
sepid_message_get_prefix (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->prefix;
}

gchar *
sepid_message_get_command (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->command;
}

GArray *
sepid_message_get_parameters (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->parameters;
}

gchar *
sepid_message_get_trailing_parameter (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->trailing_parameter;
}

SepidClient *
sepid_message_get_sender (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->sender;
}

SepidClient *
sepid_message_get_recipient (SepidMessage *self)
{
    g_return_val_if_fail (SEPID_IS_MESSAGE (self), NULL);

    return self->recipient;
}

SepidMessage *
sepid_message_new (const gchar *message,
                   SepidClient *sender,
                   SepidClient *recipient)
{
    glong length;

    length = g_utf8_strlen (message, -1);

    g_return_val_if_fail (message != NULL, NULL);
    g_return_val_if_fail (length > 0, NULL);
    g_return_val_if_fail (length <= 512, NULL);

    return g_object_new (SEPID_TYPE_MESSAGE,
                         "message", message,
                         "sender", sender,
                         "recipient", recipient,
                         NULL);
}
