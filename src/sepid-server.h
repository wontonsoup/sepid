#pragma once

#include "sepid-client.h"
#include "sepid-message.h"

#include <gio/gio.h>

#define SEPID_TYPE_SERVER (sepid_server_get_type ())

G_DECLARE_FINAL_TYPE (SepidServer, sepid_server,
                      SEPID, SERVER,
                      GThreadedSocketService)


gboolean sepid_server_nick_is_in_use (SepidServer *,
                                      gchar *);

void sepid_server_register_client (SepidServer *,
                                   SepidClient *);

void sepid_server_add_client_to_channel (SepidServer *,
                                         SepidClient *,
                                         gchar *);
void sepid_server_remove_client_from_channel (SepidServer *,
                                              SepidClient *,
                                              const gchar *);

void sepid_server_send_message (SepidServer *,
                                SepidMessage *);

void sepid_server_send_message_to_channel (SepidServer *,
                                           const gchar *,
                                           SepidMessage *);

SepidServer *sepid_server_new (int);
