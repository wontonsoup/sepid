#include "../sepid-message-handler.h"

#define SEPID_TYPE_PRIVMSG_MESSAGE_HANDLER (sepid_privmsg_message_handler_get_type ())

G_DECLARE_FINAL_TYPE (SepidPrivmsgMessageHandler, sepid_privmsg_message_handler,
                      SEPID, PRIVMSG_MESSAGE_HANDLER, GObject)

SepidMessage *sepid_privmsg_message_handler_execute (SepidMessageHandler *,
                                                  SepidMessage *,
                                                  SepidServer *);

SepidMessageHandler *sepid_privmsg_message_handler_new (void);
