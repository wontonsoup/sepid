#include "sepid-join-message-handler.h"

struct _SepidJoinMessageHandler
{
    GObject parent_instance;
};

static void
sepid_message_handler_iface_init (SepidMessageHandlerInterface *iface)
{
    iface->execute = sepid_join_message_handler_execute;
}

G_DEFINE_TYPE_WITH_CODE (SepidJoinMessageHandler, sepid_join_message_handler,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEPID_TYPE_MESSAGE_HANDLER,
                                                sepid_message_handler_iface_init))

static void
sepid_join_message_handler_class_init (SepidJoinMessageHandlerClass *klass)
{
    (void)klass;
}

static void
sepid_join_message_handler_init (SepidJoinMessageHandler *self)
{
    (void)self;
}

SepidMessage *
sepid_join_message_handler_execute (SepidMessageHandler *self,
                                    SepidMessage        *message,
                                    SepidServer         *server)
{
    GArray *parameters;
    gchar *command;
    SepidClient *sender;
    gchar *channel;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    command = sepid_message_get_command (message);
    g_return_val_if_fail (g_strcmp0 (command, "JOIN") == 0, NULL);

    sender = sepid_message_get_sender (message);
    g_return_val_if_fail (sepid_client_is_registered (sender), NULL);

    parameters = sepid_message_get_parameters (message);

    if (parameters->len != 1)
    {
        /* Silently ignore :^) */
        return NULL;
    }

    channel = g_array_index (parameters, gchar *, 0);
    if (!g_str_has_prefix (channel, "#"))
    {
        return NULL;
    }

    sepid_server_add_client_to_channel (server, sender, channel);

    return NULL;
}

SepidMessageHandler *
sepid_join_message_handler_new (void)
{
    return g_object_new (SEPID_TYPE_JOIN_MESSAGE_HANDLER, NULL);
}
