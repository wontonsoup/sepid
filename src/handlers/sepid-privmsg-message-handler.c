#include "sepid-privmsg-message-handler.h"

struct _SepidPrivmsgMessageHandler
{
    GObject parent_instance;
};

static void
sepid_message_handler_iface_init (SepidMessageHandlerInterface *iface)
{
    iface->execute = sepid_privmsg_message_handler_execute;
}

G_DEFINE_TYPE_WITH_CODE (SepidPrivmsgMessageHandler,
                         sepid_privmsg_message_handler,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEPID_TYPE_MESSAGE_HANDLER,
                                                sepid_message_handler_iface_init))

static void
sepid_privmsg_message_handler_class_init (SepidPrivmsgMessageHandlerClass *klass)
{
    (void)klass;
}

static void
sepid_privmsg_message_handler_init (SepidPrivmsgMessageHandler *self)
{
    (void)self;
}

SepidMessage *
sepid_privmsg_message_handler_execute (SepidMessageHandler *self,
                                       SepidMessage        *message,
                                       SepidServer         *server)
{
    gchar *command;
    SepidClient *sender;
    GArray *parameters;
    const gchar *trailing_parameter;
    g_autofree gchar *message_text = NULL;
    gchar *channel;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    command = sepid_message_get_command (message);
    g_return_val_if_fail (g_strcmp0 (command, "PRIVMSG") == 0, NULL);

    sender = sepid_message_get_sender (message);
    g_return_val_if_fail (sepid_client_is_registered (sender), NULL);

    parameters = sepid_message_get_parameters (message);
    trailing_parameter = sepid_message_get_trailing_parameter (message);

    if (parameters->len == 0)
    {
        message_text = g_strdup_printf (": 461 %s :Not enough parameters\r\n",
                                        sepid_client_get_nick (sender));

        return sepid_message_new (message_text, NULL, sender);
    }
    else if (parameters->len == 1 && trailing_parameter == NULL)
    {
        message_text = g_strdup_printf (": 412 %s :No text to send\r\n",
                                        sepid_client_get_nick (sender));

        return sepid_message_new (message_text, NULL, sender);
    }
    else if (parameters->len == 1 && trailing_parameter != NULL)
    {
        message_text = g_strdup_printf (": 411 %s "
                                        ":No recipient given (PRIVMSG)\r\n",
                                        sepid_client_get_nick (sender));

        return sepid_message_new (message_text, NULL, sender);
    }

    if (!g_str_has_prefix (g_array_index (parameters, gchar *, 0), "#"))
    {
        /* Easier to just allow channel comms at the moment */
        return NULL;
    }

    channel = g_array_index (parameters, gchar *, 0);

    {
        g_autoptr (SepidMessage) message = NULL;

        message_text = g_strdup_printf (":%s!~%s@%s PRIVMSG %s :%s\r\n",
                                        sepid_client_get_nick (sender),
                                        sepid_client_get_username (sender),
                                        sepid_client_get_host (sender),
                                        channel,
                                        trailing_parameter);

        message = sepid_message_new (message_text, sender, NULL);

        sepid_server_send_message_to_channel (server, channel, message);
    }

    return NULL;
}

SepidMessageHandler *
sepid_privmsg_message_handler_new (void)
{
    return g_object_new (SEPID_TYPE_PRIVMSG_MESSAGE_HANDLER, NULL);
}
