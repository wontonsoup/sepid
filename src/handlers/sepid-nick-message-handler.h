#include "../sepid-message-handler.h"

#define SEPID_TYPE_NICK_MESSAGE_HANDLER (sepid_nick_message_handler_get_type ())

G_DECLARE_FINAL_TYPE (SepidNickMessageHandler, sepid_nick_message_handler,
                      SEPID, NICK_MESSAGE_HANDLER, GObject)

SepidMessage *sepid_nick_message_handler_execute (SepidMessageHandler *,
                                                  SepidMessage *,
                                                  SepidServer *);

SepidMessageHandler *sepid_nick_message_handler_new (void);
