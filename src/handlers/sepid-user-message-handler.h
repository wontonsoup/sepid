#include "../sepid-message-handler.h"

#define SEPID_TYPE_USER_MESSAGE_HANDLER (sepid_user_message_handler_get_type ())

G_DECLARE_FINAL_TYPE (SepidUserMessageHandler, sepid_user_message_handler,
                      SEPID, USER_MESSAGE_HANDLER, GObject)

SepidMessage *sepid_user_message_handler_execute (SepidMessageHandler *,
                                                  SepidMessage *,
                                                  SepidServer *);

SepidMessageHandler *sepid_user_message_handler_new (void);
