#include "../sepid-message-handler.h"

#define SEPID_TYPE_QUIT_MESSAGE_HANDLER (sepid_quit_message_handler_get_type ())

G_DECLARE_FINAL_TYPE (SepidQuitMessageHandler, sepid_quit_message_handler,
                      SEPID, QUIT_MESSAGE_HANDLER, GObject)

SepidMessage *sepid_quit_message_handler_execute (SepidMessageHandler *,
                                                  SepidMessage *,
                                                  SepidServer *);

SepidMessageHandler *sepid_quit_message_handler_new (void);
