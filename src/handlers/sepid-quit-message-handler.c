#include "sepid-quit-message-handler.h"

struct _SepidQuitMessageHandler
{
    GObject parent_instance;
};

static void
sepid_message_handler_iface_init (SepidMessageHandlerInterface *iface)
{
    iface->execute = sepid_quit_message_handler_execute;
}

G_DEFINE_TYPE_WITH_CODE (SepidQuitMessageHandler, sepid_quit_message_handler,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEPID_TYPE_MESSAGE_HANDLER,
                                                sepid_message_handler_iface_init))

static void
sepid_quit_message_handler_class_init (SepidQuitMessageHandlerClass *klass)
{
    (void)klass;
}

static void
sepid_quit_message_handler_init (SepidQuitMessageHandler *self)
{
    (void)self;
}

SepidMessage *
sepid_quit_message_handler_execute (SepidMessageHandler *self,
                                    SepidMessage        *message,
                                    SepidServer         *server)
{
    GArray *parameters;
    gchar *trailing_parameter;
    gchar *command;
    SepidClient *sender;
    const GArray *channels;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    command = sepid_message_get_command (message);
    g_return_val_if_fail (g_strcmp0 (command, "QUIT") == 0, NULL);

    parameters = sepid_message_get_parameters (message);
    trailing_parameter = sepid_message_get_trailing_parameter (message);
    sender = sepid_message_get_sender (message);
    channels = sepid_client_get_memberships (sender);

    g_return_val_if_fail (sepid_client_is_registered (sender), NULL);

    for (guint i = 0; i < channels->len; i++)
    {
        SepidChannel *channel;
        const gchar *channel_name;
        const GArray *clients;

        channel = g_array_index (channels, SepidChannel *, i);
        channel_name = sepid_channel_get_name (channel);

        sepid_server_remove_client_from_channel (server, sender, channel_name);

        clients = sepid_channel_get_clients (channel);
        for (guint i = 0; i < clients->len; i++)
        {
            SepidClient *client;
            g_autofree gchar* message_text;
            SepidMessage *message;

            client = g_array_index (clients, SepidClient *, i);
            message_text =
                g_strdup_printf (":%s!~%s@%s QUIT :%s\r\n",
                                 sepid_client_get_nick (sender),
                                 sepid_client_get_username (sender),
                                 sepid_client_get_host (sender),
                                 (trailing_parameter != NULL ?
                                  trailing_parameter : ""));
            message = sepid_message_new (message_text, NULL, client);

            sepid_server_send_message (server, message);
        }
    }

    return NULL;
}

SepidMessageHandler *
sepid_quit_message_handler_new (void)
{
    return g_object_new (SEPID_TYPE_QUIT_MESSAGE_HANDLER, NULL);
}
