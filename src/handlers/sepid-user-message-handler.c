#include "sepid-user-message-handler.h"

struct _SepidUserMessageHandler
{
    GObject parent_instance;
};

static void
sepid_message_handler_iface_init (SepidMessageHandlerInterface *iface)
{
    iface->execute = sepid_user_message_handler_execute;
}

G_DEFINE_TYPE_WITH_CODE (SepidUserMessageHandler, sepid_user_message_handler,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEPID_TYPE_MESSAGE_HANDLER,
                                                sepid_message_handler_iface_init))

static void
sepid_user_message_handler_class_init (SepidUserMessageHandlerClass *klass)
{
    (void)klass;
}

static void
sepid_user_message_handler_init (SepidUserMessageHandler *self)
{
    (void)self;
}

SepidMessage *
sepid_user_message_handler_execute (SepidMessageHandler *self,
                                    SepidMessage        *message,
                                    SepidServer         *server)
{
    GArray *parameters;
    SepidClient *sender;
    gchar *command;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    command = sepid_message_get_command (message);
    g_return_val_if_fail (g_strcmp0 (command, "USER") == 0, NULL);

    parameters = sepid_message_get_parameters (message);

    g_return_val_if_fail (parameters->len == 4, NULL);

    sender = sepid_message_get_sender (message);

    if (sepid_client_is_registered (sender))
    {
        return sepid_message_new ("462 :Unauthorized command (already registered)\r\n",
                                  NULL, sender);
    }

    sepid_client_set_username (sender, g_array_index (parameters, gchar *, 0));
    /* sepid_client_set_realname (sender, g_array_index (parameters, 3)); */

    if (sepid_client_get_nick (sender) != NULL)
    {
        sepid_server_register_client (server, sender);
    }

    return NULL;
}

SepidMessageHandler *
sepid_user_message_handler_new (void)
{
    return g_object_new (SEPID_TYPE_USER_MESSAGE_HANDLER, NULL);
}
