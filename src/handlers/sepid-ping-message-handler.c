#include "sepid-ping-message-handler.h"

struct _SepidPingMessageHandler
{
    GObject parent_instance;
};

static void
sepid_message_handler_iface_init (SepidMessageHandlerInterface *iface)
{
    iface->execute = sepid_ping_message_handler_execute;
}

G_DEFINE_TYPE_WITH_CODE (SepidPingMessageHandler, sepid_ping_message_handler,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEPID_TYPE_MESSAGE_HANDLER,
                                                sepid_message_handler_iface_init))

static void
sepid_ping_message_handler_class_init (SepidPingMessageHandlerClass *klass)
{
    (void)klass;
}

static void
sepid_ping_message_handler_init (SepidPingMessageHandler *self)
{
    (void)self;
}

SepidMessage *
sepid_ping_message_handler_execute (SepidMessageHandler *self,
                                    SepidMessage        *message,
                                    SepidServer         *server)
{
    GArray *parameters;
    gchar *command;
    SepidClient *sender;
    g_autofree gchar *message_text = NULL;

    (void)server;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    command = sepid_message_get_command (message);
    g_return_val_if_fail (g_strcmp0 (command, "PING") == 0, NULL);

    sender = sepid_message_get_sender (message);
    g_return_val_if_fail (sepid_client_is_registered (sender), NULL);

    parameters = sepid_message_get_parameters (message);

    if (parameters->len != 1)
    {
        return NULL;
    }

    message_text = g_strdup_printf (": PONG %s\r\n",
                                    g_array_index (parameters, gchar *, 0));

    return sepid_message_new (message_text, NULL, sender);
}

SepidMessageHandler *
sepid_ping_message_handler_new (void)
{
    return g_object_new (SEPID_TYPE_PING_MESSAGE_HANDLER, NULL);
}
