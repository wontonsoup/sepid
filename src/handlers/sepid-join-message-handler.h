#include "../sepid-message-handler.h"

#define SEPID_TYPE_JOIN_MESSAGE_HANDLER (sepid_join_message_handler_get_type ())

G_DECLARE_FINAL_TYPE (SepidJoinMessageHandler, sepid_join_message_handler,
                      SEPID, JOIN_MESSAGE_HANDLER, GObject)

SepidMessage *sepid_join_message_handler_execute (SepidMessageHandler *,
                                                  SepidMessage *,
                                                  SepidServer *);

SepidMessageHandler *sepid_join_message_handler_new (void);
