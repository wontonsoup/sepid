#include "../sepid-message-handler.h"

#define SEPID_TYPE_PING_MESSAGE_HANDLER (sepid_ping_message_handler_get_type ())

G_DECLARE_FINAL_TYPE (SepidPingMessageHandler, sepid_ping_message_handler,
                      SEPID, PING_MESSAGE_HANDLER, GObject)

SepidMessage *sepid_ping_message_handler_execute (SepidMessageHandler *,
                                                  SepidMessage *,
                                                  SepidServer *);

SepidMessageHandler *sepid_ping_message_handler_new (void);
