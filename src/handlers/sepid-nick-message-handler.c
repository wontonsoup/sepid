#include "sepid-nick-message-handler.h"

struct _SepidNickMessageHandler
{
    GObject parent_instance;
};

static void
sepid_message_handler_iface_init (SepidMessageHandlerInterface *iface)
{
    iface->execute = sepid_nick_message_handler_execute;
}

G_DEFINE_TYPE_WITH_CODE (SepidNickMessageHandler, sepid_nick_message_handler,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (SEPID_TYPE_MESSAGE_HANDLER,
                                                sepid_message_handler_iface_init))

static void
sepid_nick_message_handler_class_init (SepidNickMessageHandlerClass *klass)
{
    (void)klass;
}

static void
sepid_nick_message_handler_init (SepidNickMessageHandler *self)
{
    (void)self;
}

SepidMessage *
sepid_nick_message_handler_execute (SepidMessageHandler *self,
                                    SepidMessage        *message,
                                    SepidServer         *server)
{
    GArray *parameters;
    gchar *command;
    SepidClient *sender;
    gchar *nick;
    gchar *old_nick;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    command = sepid_message_get_command (message);
    g_return_val_if_fail (g_strcmp0 (command, "NICK") == 0, NULL);

    parameters = sepid_message_get_parameters (message);
    sender = sepid_message_get_sender (message);

    if (parameters->len == 0)
    {
        return sepid_message_new ("431 :No nickname given\r\n", NULL, sender);
    }

    nick = g_array_index (parameters, gchar *, 0);

    if (sepid_server_nick_is_in_use (server, nick))
    {
        g_autofree gchar *message;

        message = g_strdup_printf ("433 %s :Nickname is already in use\r\n",
                                   nick);

        return sepid_message_new (message, NULL, sender);
    }

    old_nick = sepid_client_get_nick (sender);
    sepid_client_set_nick (sender, nick);

    if (!sepid_client_is_registered (sender) &&
        sepid_client_get_username (sender) != NULL)
    {
        sepid_server_register_client (server, sender);
    }
    else if (sepid_client_is_registered (sender))
    {
        g_autofree gchar *message_text = NULL;

        message_text = g_strdup_printf (":%s!~%s@%s NICK :%s\r\n",
                                       old_nick,
                                       sepid_client_get_username (sender),
                                       sepid_client_get_host (sender),
                                       nick);

        return sepid_message_new (message_text, NULL, sender);
    }

    return NULL;
}

SepidMessageHandler *
sepid_nick_message_handler_new (void)
{
    return g_object_new (SEPID_TYPE_NICK_MESSAGE_HANDLER, NULL);
}
