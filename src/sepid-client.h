#pragma once

#include "sepid-channel.h"

#include <gio/gio.h>

#define SEPID_TYPE_CLIENT (sepid_client_get_type ())

G_DECLARE_FINAL_TYPE (SepidClient, sepid_client, SEPID, CLIENT, GObject)

const GArray *sepid_client_get_memberships (SepidClient *);
void sepid_client_add_membership (SepidClient *,
                                  SepidChannel *);
void sepid_client_remove_membership (SepidClient *,
                                     SepidChannel *);

GSocketConnection *sepid_client_get_socket_connection (SepidClient *);

gchar *sepid_client_get_host (SepidClient *);

gchar *sepid_client_get_username (SepidClient *);
void sepid_client_set_username (SepidClient *,
                                gchar *);

gchar *sepid_client_get_nick (SepidClient *);
void sepid_client_set_nick (SepidClient *,
                            gchar *);

gboolean sepid_client_is_registered (SepidClient *);
void sepid_client_set_is_registered (SepidClient *);

SepidClient *sepid_client_new (GSocketConnection *);
