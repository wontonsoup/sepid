#include "sepid-server.h"

#include <stdlib.h>

int
main (void)
{
    g_autoptr (SepidServer) server = NULL;
    gboolean port_added;
    GMainContext *main_context;

    server = sepid_server_new (-1);

    port_added = g_socket_listener_add_inet_port (G_SOCKET_LISTENER (server),
                                                  6667, NULL, NULL);
    if (!port_added)
    {
        return EXIT_FAILURE;
    }

    main_context = g_main_context_default ();

    while (g_socket_service_is_active (G_SOCKET_SERVICE (server)))
    {
        g_main_context_iteration (main_context, FALSE);
    }

    return EXIT_SUCCESS;
}
