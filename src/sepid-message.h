#pragma once

#include "sepid-client.h"

#include <glib.h>
#include <glib-object.h>

#define SEPID_TYPE_MESSAGE (sepid_message_get_type ())

G_DECLARE_FINAL_TYPE (SepidMessage, sepid_message, SEPID, MESSAGE, GObject)

gchar *sepid_message_get_message (SepidMessage *);

gchar *sepid_message_get_prefix (SepidMessage *);
gchar *sepid_message_get_command (SepidMessage *);
GArray *sepid_message_get_parameters (SepidMessage *);
gchar *sepid_message_get_trailing_parameter (SepidMessage *);

SepidClient *sepid_message_get_sender (SepidMessage *);
SepidClient *sepid_message_get_recipient (SepidMessage *);

SepidMessage *sepid_message_new (const gchar *,
                                 SepidClient *,
                                 SepidClient *);
