#include "sepid-server.h"

#include "handlers/sepid-join-message-handler.h"
#include "handlers/sepid-nick-message-handler.h"
#include "handlers/sepid-ping-message-handler.h"
#include "handlers/sepid-privmsg-message-handler.h"
#include "handlers/sepid-quit-message-handler.h"
#include "handlers/sepid-user-message-handler.h"

#include "sepid-channel.h"
#include "sepid-client.h"
#include "sepid-message.h"

#include <string.h>

struct _SepidServer
{
    GThreadedSocketService parent_instance;

    GAsyncQueue *message_queue;
    GAsyncQueue *send_queue;
    GHashTable *message_handlers;

    GThread *service_thread;
    GThread *send_thread;

    GHashTable *channels;
};

G_DEFINE_TYPE (SepidServer, sepid_server, G_TYPE_THREADED_SOCKET_SERVICE)

static gboolean
run (GThreadedSocketService *service,
     GSocketConnection      *connection,
     GObject                *source_object)
{
    SepidServer *self;
    g_autoptr (SepidClient) client = NULL;
    GSocket *socket;
    g_autoptr (GIOChannel) io_channel = NULL;
    GString buffer = { NULL, 0, 0 };
    gsize terminator_pos;
    GIOStatus io_status;
    SepidMessage *message;

    self = SEPID_SERVER (service);
    client = sepid_client_new (connection);
    socket = g_socket_connection_get_socket (connection);
    io_channel = g_io_channel_unix_new (g_socket_get_fd (socket));

    (void)service;
    (void)source_object;

    g_debug ("Client %s connected", sepid_client_get_host (client));

    g_io_channel_set_line_term (io_channel, "\r\n", -1);

    do
    {
        io_status = g_io_channel_read_line_string (io_channel,
                                                   &buffer, &terminator_pos,
                                                   NULL);

        if (buffer.len == 0)
        {
            continue;
        }

        g_string_erase (&buffer, terminator_pos, -1);

        message = sepid_message_new (buffer.str, client, NULL);

        g_debug ("Received \"%s\" from client %s",
                 sepid_message_get_message (message),
                 sepid_client_get_host (client));

        g_async_queue_push (self->message_queue, message);
    } while (io_status == G_IO_STATUS_NORMAL || io_status == G_IO_STATUS_AGAIN);

    if (buffer.len > 0)
    {
        g_string_erase (&buffer, 0, -1);
    }

    g_debug ("Client %s disconnected", sepid_client_get_host (client));

    return FALSE;
}

static void
finalize (GObject *object)
{
    SepidServer *self;

    self = SEPID_SERVER (object);

    g_clear_pointer (&self->message_queue, g_async_queue_unref);
    g_clear_pointer (&self->send_queue, g_async_queue_unref);
    g_clear_pointer (&self->message_handlers, g_hash_table_destroy);

    G_OBJECT_CLASS (sepid_server_parent_class)->finalize (object);
}

static void
sepid_server_class_init (SepidServerClass *klass)
{
    GObjectClass *object_class;
    GThreadedSocketServiceClass *service_class;

    object_class = G_OBJECT_CLASS (klass);
    service_class = G_THREADED_SOCKET_SERVICE_CLASS (klass);

    object_class->finalize = finalize;

    service_class->run = run;
}

static gpointer
service_thread_func (gpointer data)
{
    SepidServer *self;

    self = SEPID_SERVER (data);

    while (g_socket_service_is_active (G_SOCKET_SERVICE (self)))
    {
        g_autoptr (SepidMessage) message = NULL;
        SepidMessageHandler *handler;

        message = g_async_queue_pop (self->message_queue);
        handler = g_hash_table_lookup (self->message_handlers,
                                       sepid_message_get_command (message));

        if (handler != NULL)
        {
            SepidMessage *response = sepid_message_handler_execute (handler,
                                                                    message,
                                                                    self);

            if (response != NULL)
            {
                g_async_queue_push (self->send_queue, response);
            }
        }
    }

    return NULL;
}

static gpointer
send_thread_func (gpointer data)
{
    SepidServer *self;

    self = SEPID_SERVER (data);

    while (g_socket_service_is_active (G_SOCKET_SERVICE (self)))
    {
        g_autoptr (SepidMessage) message = NULL;
        SepidClient *recipient;
        GSocketConnection *connection;
        GIOStream *io_stream;
        GOutputStream *output_stream;
        gchar *message_text;

        message = g_async_queue_pop (self->send_queue);
        recipient = sepid_message_get_recipient (message);
        connection = sepid_client_get_socket_connection (recipient);
        io_stream = G_IO_STREAM (connection);
        output_stream = g_io_stream_get_output_stream (io_stream);
        message_text = sepid_message_get_message (message);

        g_debug ("Sending \"%s\" to client %s",
                 message_text, sepid_client_get_host (recipient));

        g_output_stream_write_all (output_stream,
                                   message_text, strlen (message_text),
                                   NULL, NULL, NULL);

    }

    return NULL;
}

static void
sepid_server_init (SepidServer *self)
{
    self->message_queue = g_async_queue_new_full (g_object_unref);
    self->send_queue = g_async_queue_new_full (g_object_unref);
    self->message_handlers = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                    g_free, g_object_unref);
    self->service_thread = g_thread_new (NULL, service_thread_func, self);
    self->send_thread = g_thread_new (NULL, send_thread_func, self);
    self->channels = g_hash_table_new_full (g_str_hash, g_str_equal,
                                            g_free, g_object_unref);

    g_hash_table_insert (self->message_handlers,
                         g_strdup ("USER"),
                         sepid_user_message_handler_new ());
    g_hash_table_insert (self->message_handlers,
                         g_strdup ("NICK"),
                         sepid_nick_message_handler_new ());
    g_hash_table_insert (self->message_handlers,
                         g_strdup ("JOIN"),
                         sepid_join_message_handler_new ());
    g_hash_table_insert (self->message_handlers,
                         g_strdup ("PING"),
                         sepid_ping_message_handler_new ());
    g_hash_table_insert (self->message_handlers,
                         g_strdup ("QUIT"),
                         sepid_quit_message_handler_new ());
    g_hash_table_insert (self->message_handlers,
                         g_strdup ("PRIVMSG"),
                         sepid_privmsg_message_handler_new ());
}

gboolean
sepid_server_nick_is_in_use (SepidServer *self,
                             gchar       *nick)
{
    return FALSE;
}

void
sepid_server_register_client (SepidServer *self,
                              SepidClient *client)
{
    g_autofree gchar *message_text = NULL;

    g_debug ("Registering client %s", sepid_client_get_host (client));

    sepid_client_set_is_registered (client);

    message_text = g_strdup_printf (": "
                                    "001 %s "
                                    ":Welcome to the Internet Relay Network "
                                    "%s!~%s@%s"
                                    "\r\n",
                                    sepid_client_get_nick (client),
                                    sepid_client_get_nick (client),
                                    sepid_client_get_username (client),
                                    sepid_client_get_host (client));

    g_async_queue_push (self->send_queue, sepid_message_new (message_text,
                                                             NULL, client));
}

void
sepid_server_add_client_to_channel (SepidServer *self,
                                    SepidClient *client,
                                    gchar       *channel_name)
{
    SepidChannel *channel;
    g_autofree gchar *join_message_text = NULL;
    g_autofree gchar *names_message_text = NULL;
    g_autofree gchar *end_of_names_message_text = NULL;
    const GArray *clients;

    channel = g_hash_table_lookup (self->channels, channel_name);
    if (channel == NULL)
    {
        channel = sepid_channel_new (channel_name);

        sepid_channel_add_client (channel, client);

        g_hash_table_insert (self->channels, channel_name, channel);
    }
    else
    {
        if (sepid_channel_contains_client (channel, client))
        {
            return;
        }

        sepid_channel_add_client (channel, client);
    }

    join_message_text = g_strdup_printf (":%s!~%s@%s "
                                         "JOIN "
                                         "%s"
                                         "\r\n",
                                         sepid_client_get_nick (client),
                                         sepid_client_get_username (client),
                                         sepid_client_get_host (client),
                                         sepid_channel_get_name (channel));

    clients = sepid_channel_get_clients (channel);
    for (guint i = 0; i < clients->len; i++)
    {
        SepidClient *channel_client;

        channel_client = g_array_index (clients, SepidClient *, i);
        names_message_text =
            g_strdup_printf (": 353 %s = %s :%s\r\n",
                             sepid_client_get_nick (client),
                             sepid_channel_get_name (channel),
                             sepid_client_get_nick (channel_client));

        g_async_queue_push (self->send_queue,
                            sepid_message_new (join_message_text,
                                               NULL, channel_client));
        g_async_queue_push (self->send_queue,
                            sepid_message_new (names_message_text,
                                               NULL, client));
    }

    end_of_names_message_text =
        g_strdup_printf (": 366 %s %s :End of /NAMES list.\r\n",
                         sepid_client_get_nick (client),
                         sepid_channel_get_name (channel));

    g_async_queue_push (self->send_queue,
                        sepid_message_new (end_of_names_message_text,
                                           NULL, client));
}

void
sepid_server_remove_client_from_channel (SepidServer *self,
                                         SepidClient *client,
                                         const gchar *channel_name)
{
    SepidChannel *channel;

    g_return_if_fail (SEPID_IS_SERVER (self));

    channel = g_hash_table_lookup (self->channels, channel_name);
    if (channel == NULL)
    {
        return;
    }

    sepid_channel_remove_client (channel, client);
}

void
sepid_server_send_message (SepidServer  *self,
                           SepidMessage *message)
{
    g_return_if_fail (SEPID_IS_SERVER (self));

    g_async_queue_push (self->send_queue, message);
}

void
sepid_server_send_message_to_channel (SepidServer  *self,
                                      const gchar  *channel_name,
                                      SepidMessage *message)
{
    SepidChannel *channel;
    const GArray *clients;

    g_return_if_fail (SEPID_IS_SERVER (self));

    channel = g_hash_table_lookup (self->channels, channel_name);
    if (channel == NULL)
    {
        return;
    }

    clients = sepid_channel_get_clients (channel);
    for (guint i = 0; i < clients->len; i++)
    {
        SepidClient *client;
        const gchar *message_text;

        client = g_array_index (clients, SepidClient *, i);
        if (client == sepid_message_get_sender (message))
        {
            continue;
        }

        message_text = sepid_message_get_message (message);

        g_async_queue_push (self->send_queue,
                            sepid_message_new (message_text, NULL, client));
    }
}

SepidServer *
sepid_server_new (int max_threads)
{
    return g_object_new (SEPID_TYPE_SERVER,
                         "max-threads", max_threads,
                         NULL);
}
