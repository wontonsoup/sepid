#include "sepid-client.h"

struct _SepidClient
{
    GObject parent_instance;

    GSocketConnection *connection;

    gchar *host;
    gchar *username;
    gchar *nick;
    gchar *realname;

    gboolean registered;

    GArray *channels;
};

G_DEFINE_TYPE (SepidClient, sepid_client, G_TYPE_OBJECT)

enum
{
    PROP_CONNECTION = 1,
    N_PROPERTIES
};

static GParamSpec *pspecs[N_PROPERTIES] = { 0 };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    SepidClient *self;
    g_autoptr (GSocketAddress) socket_address = NULL;
    GInetSocketAddress *inet_socket_address;
    GInetAddress *inet_address;

    self = SEPID_CLIENT (object);

    switch (property_id)
    {
        case PROP_CONNECTION:
        {
            self->connection = G_SOCKET_CONNECTION (g_value_dup_object (value));

            socket_address = g_socket_connection_get_remote_address (self->connection,
                                                                     NULL);

            g_assert (socket_address != NULL);

            inet_socket_address = G_INET_SOCKET_ADDRESS (socket_address);
            inet_address = g_inet_socket_address_get_address (inet_socket_address);

            self->host = g_inet_address_to_string (inet_address);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
}

static void
finalize (GObject *object)
{
    SepidClient *self;

    self = SEPID_CLIENT (object);

    g_clear_object (&self->connection);
    g_clear_pointer (&self->host, g_free);
    g_clear_pointer (&self->username, g_free);
    (void)g_array_free (self->channels, FALSE);

    G_OBJECT_CLASS (sepid_client_parent_class)->finalize (object);
}

static void
sepid_client_class_init (SepidClientClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    pspecs[PROP_CONNECTION] =
        g_param_spec_object ("connection", "Connection",
                             "Connection",
                             G_TYPE_SOCKET_CONNECTION,
                             G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties (object_class, N_PROPERTIES, pspecs);
}

static void
sepid_client_init (SepidClient *self)
{
    self->connection = NULL;
    self->host = NULL;
    self->username = NULL;
    self->registered = FALSE;
    self->channels = g_array_new (FALSE, FALSE, sizeof (SepidChannel *));
}

const GArray *
sepid_client_get_memberships (SepidClient *self)
{
    g_return_val_if_fail (SEPID_IS_CLIENT (self), NULL);

    return self->channels;
}

void
sepid_client_add_membership (SepidClient  *self,
                             SepidChannel *channel)
{
    g_return_if_fail (SEPID_IS_CLIENT (self));

    for (guint i = 0; i < self->channels->len; i++)
    {
        if (g_array_index (self->channels, SepidChannel *, i) == channel)
        {
            return;
        }
    }

    g_array_append_val (self->channels, channel);
}

void
sepid_client_remove_membership (SepidClient  *self,
                                SepidChannel *channel)
{
    g_return_if_fail (SEPID_IS_CLIENT (self));

    for (guint i = 0; i < self->channels->len; i++)
    {
        if (g_array_index (self->channels, SepidChannel *, i) == channel)
        {
            g_array_remove_index (self->channels, i);
        }
    }
}

GSocketConnection *
sepid_client_get_socket_connection (SepidClient *self)
{
    g_return_val_if_fail (SEPID_IS_CLIENT (self), NULL);

    return self->connection;
}

gchar *
sepid_client_get_host (SepidClient *self)
{
    g_return_val_if_fail (SEPID_IS_CLIENT (self), NULL);

    return self->host;
}

gchar *
sepid_client_get_username (SepidClient *self)
{
    g_return_val_if_fail (SEPID_IS_CLIENT (self), NULL);

    return self->username;
}

void
sepid_client_set_username (SepidClient *self,
                           gchar       *username)
{
    g_return_if_fail (SEPID_IS_CLIENT (self));
    g_return_if_fail (self->username == NULL);

    g_debug ("Setting username \"%s\" for client %s", username, self->host);

    self->username = g_strdup (username);
}

gchar *
sepid_client_get_nick (SepidClient *self)
{
    g_return_val_if_fail (SEPID_IS_CLIENT (self), NULL);

    return self->nick;
}

void
sepid_client_set_nick (SepidClient *self,
                       gchar       *nick)
{
    g_return_if_fail (SEPID_IS_CLIENT (self));

    g_debug ("Setting nick \"%s\" for client %s", nick, self->host);

    self->nick = g_strdup (nick);
}

gboolean
sepid_client_is_registered (SepidClient *self)
{
    g_return_val_if_fail (SEPID_IS_CLIENT (self), FALSE);

    return self->registered;
}

void
sepid_client_set_is_registered (SepidClient *self)
{
    g_return_if_fail (SEPID_IS_CLIENT (self));

    self->registered = TRUE;
}

SepidClient *
sepid_client_new (GSocketConnection *connection)
{
    return g_object_new (SEPID_TYPE_CLIENT,
                         "connection", connection,
                         NULL);
}
