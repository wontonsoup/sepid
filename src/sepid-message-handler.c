#include "sepid-message-handler.h"

G_DEFINE_INTERFACE (SepidMessageHandler, sepid_message_handler, G_TYPE_OBJECT)

static void
sepid_message_handler_default_init (SepidMessageHandlerInterface *iface)
{
    (void)iface;
}

SepidMessage *
sepid_message_handler_execute (SepidMessageHandler *self,
                               SepidMessage        *message,
                               SepidServer         *server)
{
    SepidMessageHandlerInterface *iface;

    g_return_val_if_fail (SEPID_IS_MESSAGE_HANDLER (self), NULL);

    iface = SEPID_MESSAGE_HANDLER_GET_IFACE (self);

    g_return_val_if_fail (iface->execute != NULL, NULL);

    return iface->execute (self, message, server);
}
