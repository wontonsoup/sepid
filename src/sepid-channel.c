#include "sepid-channel.h"

struct _SepidChannel
{
    GObject parent_instance;

    gchar *name;
    /* gchar *topic; */

    GArray *clients;
};

G_DEFINE_TYPE (SepidChannel, sepid_channel, G_TYPE_OBJECT)

enum
{
    PROP_NAME = 1,
    N_PROPERTIES
};

static GParamSpec *pspecs[N_PROPERTIES] = { 0 };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    SepidChannel *self;

    self = SEPID_CHANNEL (object);

    switch (property_id)
    {
        case PROP_NAME:
        {
            self->name = g_value_dup_string (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    SepidChannel *self;

    self = SEPID_CHANNEL (object);

    switch (property_id)
    {
        case PROP_NAME:
        {
            g_value_set_string (value, self->name);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    SepidChannel *self;

    self = SEPID_CHANNEL (object);

    g_clear_pointer (&self->name, g_free);
    (void)g_array_free (self->clients, FALSE);
}

static void
sepid_channel_class_init (SepidChannelClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    pspecs[PROP_NAME] =
        g_param_spec_string ("name",
                             "Name",
                             "Name",
                             NULL,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties (object_class, N_PROPERTIES, pspecs);
}

static void
sepid_channel_init (SepidChannel *self)
{
    self->clients = g_array_new (FALSE, FALSE, sizeof (SepidClient *));
}

const gchar *
sepid_channel_get_name (SepidChannel *self)
{
    g_return_val_if_fail (SEPID_IS_CHANNEL (self), NULL);

    return self->name;
}

const GArray *
sepid_channel_get_clients (SepidChannel *self)
{
    g_return_val_if_fail (SEPID_IS_CHANNEL (self), NULL);

    return self->clients;
}

gboolean
sepid_channel_contains_client (SepidChannel      *self,
                               const SepidClient *client)
{
    g_return_val_if_fail (SEPID_IS_CHANNEL (self), FALSE);

    for (guint i = 0; i < self->clients->len; i++)
    {
        if (g_array_index (self->clients, SepidClient *, i) == client)
        {
            return TRUE;
        }
    }

    return FALSE;
}

void
sepid_channel_add_client (SepidChannel      *self,
                          const SepidClient *client)
{
    g_return_if_fail (SEPID_IS_CHANNEL (self));

    if (!sepid_channel_contains_client (self, client))
    {
        g_array_append_val (self->clients, client);
        sepid_client_add_membership (client, self);
    }
}

void sepid_channel_remove_client (SepidChannel      *self,
                                  const SepidClient *client)
{
    g_return_if_fail (SEPID_IS_CHANNEL (self));

    for (guint i = 0; i < self->clients->len; i++)
    {
        if (g_array_index (self->clients, SepidClient *, i) == client)
        {
            g_array_remove_index (self->clients, i);
            sepid_client_remove_membership (client, self);
        }
    }
}

SepidChannel *
sepid_channel_new (const gchar *name)
{
    return g_object_new (SEPID_TYPE_CHANNEL,
                         "name", name,
                         NULL);
}
