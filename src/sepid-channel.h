#pragma once

struct _SepidClient;
typedef struct _SepidClient SepidClient;

#include <glib.h>
#include <glib-object.h>

#define SEPID_TYPE_CHANNEL (sepid_channel_get_type ())

G_DECLARE_FINAL_TYPE (SepidChannel, sepid_channel, SEPID, CHANNEL, GObject)

const gchar *sepid_channel_get_name (SepidChannel *);

const GArray *sepid_channel_get_clients (SepidChannel *);

gboolean sepid_channel_contains_client (SepidChannel *,
                                        const SepidClient *);

void sepid_channel_add_client (SepidChannel *,
                               const SepidClient *);
void sepid_channel_remove_client (SepidChannel *,
                                  const SepidClient *);

SepidChannel *sepid_channel_new (const gchar *);
