#pragma once

#include "sepid-message.h"
#include "sepid-server.h"

#include <glib.h>

#define SEPID_TYPE_MESSAGE_HANDLER (sepid_message_handler_get_type ())

G_DECLARE_INTERFACE (SepidMessageHandler, sepid_message_handler,
                     SEPID, MESSAGE_HANDLER, GObject)

struct _SepidMessageHandlerInterface
{
    GTypeInterface g_iface;

    SepidMessage *(*execute) (SepidMessageHandler *,
                              SepidMessage *,
                              SepidServer *);
};

SepidMessage *sepid_message_handler_execute (SepidMessageHandler *,
                                             SepidMessage *,
                                             SepidServer *);
